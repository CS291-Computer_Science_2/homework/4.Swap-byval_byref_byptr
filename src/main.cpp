#include <iostream>
using namespace std;

void swapByPtr(int *a, int *b) {
	int x=*a;
	*a=*b;
	*b=x;
}

void swapByMem(int &c, int &d) {
	int y=c;
	c=d;
	d=y;
}

void swapByVal(int e, int f) {
	int z=e;
	e=f;
	f=z;
}

int main(int argv, char** arg) {
	/* # PASS BY POINTER # */
	int a=10, b=11;
	cout<<"Before Pointer Swap a: "<<a<<" b: "<<b<<endl;
	swapByPtr(&a,&b);
	cout<<"After Pointer Swap a: "<<a<<" b: "<<b<<endl;

	/* # PASS BY MEMORY # */
	int c=20, d=21;
	cout<<"Before Memory Swap c: "<<c<<" d: "<<d<<endl;
	swapByMem(c,d);
	cout<<"After Memory Swap c: "<<c<<" d: "<<d<<endl;

	/* # FUBAR # */
	int e=30,f=31;
	cout<<"Before ByVal Swap e: "<<e<<" f: "<<f<<endl;
	swapByVal(e,f);
	cout<<"After ByVal Swap e: "<<e<<" f: "<<f<<endl;

	system("pause");
	return 0;
}
